var http = require('http');

var server = http.createServer(function(request, response) {
    console.log('New request!!');
    response.writeHead(200, {'Content-Type' : 'text/plain'});
    response.end('Hello World !');
});

server.listen(3000);
console.log('Server listening on port 3000');
