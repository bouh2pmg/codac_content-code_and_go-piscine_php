<?
$names = array("eeee", "abc", "aaa", "dddd");

print_r(my_change_user($names));

// Result :
// array(4) {
//    [0]=>
//    string(3) "aaa"
//    [1]=>
//    string(3) "abc"
//    [2]=>
//    string(4) "dddd"
//    [3]=>
//    string(4) "eeee"
// }
