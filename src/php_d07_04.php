<?php
$warrior = new Warrior("Jean-Luc");
$warrior->moveRight();
$warrior->moveLeft();
$warrior->moveUp();
$warrior->moveDown();

$mage = new Mage("Robert");
$mage->moveRight();
$mage->moveLeft();
$mage->moveUp();
$mage->moveDown();

// displays

// "Jean-Luc: I'll engrave my name in history!"
// "Jean-Luc: moves right like a bad boy."
// "Jean-Luc: moves left like a bad boy."
// "Jean-Luc: moves up like a bad boy."
// "Jean-Luc: moves down like a bad boy."
// "Robert: May the gods be with me."
// "Robert: moves right furtively."
// "Robert: moves left furtively."
// "Robert: moves up furtively."
// "Robert: moves down furtively."
// "Robert: By the four gods, I passed away..."
// "Jean-Luc: Aarrg I can't believe I'm dead..."
