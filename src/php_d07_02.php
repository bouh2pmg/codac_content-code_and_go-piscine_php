<?php
$warrior = new Warrior("Jean-Luc");
$mage = new Mage("Robert");
$warrior->attack();
$mage->attack();

// displays
// "Jean-Luc: I'll engrave my name in history"
// "Robert: May the gods be with me."
// "Jean-Luc: I'll crush you with my hammer!"
// "Robert: Feel the power of my magic!"
// "Robert: By the four gods, I passed away..."
// "Jean-Luc: Aarrg I can't believe I'm dead..."
?>