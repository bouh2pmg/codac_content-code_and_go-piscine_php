<?php
require_once("character.class.php");

foreach ([new Character, new Character("Julien"), new Character] as $character)
{
    echo $character;
}

?>

// displays:
// My name is Character 1.
// My name is Julien.
// My name is Character 2.