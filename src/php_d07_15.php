<?php
$spaceMarine = new \Imperium\Soldier("Gessart");
$chaosSpaceMarine = new \Chaos\Soldier("Ruphen");

echo $spaceMarine,"\n";
echo $chaosSpaceMarine,"\n";

$spaceMarine->doDamage($chaosSpaceMarine);

echo $spaceMarine,"\n";
echo $chaosSpaceMarine,"\n";
?>