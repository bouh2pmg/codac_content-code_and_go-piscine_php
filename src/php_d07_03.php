$warrior = new Warrior("Jean-Luc");
$warrior->moveRight();
$warrior->moveLeft();
$warrior->moveDown();
$warrior->moveUp();

// displays
// "Jean-Luc: I'll engrave my name in history!"
// "Jean-Luc: moves right."
// "Jean-Luc: moves left."
// "Jean-Luc: moves down."
// "Jean-Luc: moves up."
// "Jean-Luc: Aarrg I can't believe I'm dead..."
