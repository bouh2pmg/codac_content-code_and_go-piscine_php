<?php
$gecko = new Gecko("Gecko");
echo ($gecko->getName() . " starts to check:\n");
$soldat = new Soldier("James Francis Ryan");
$gecko->correct($soldat);

/*
  Must display:
  Gecko starts to check:
  Test 0 : Good !
  Test 1 : KO.
  Test 2 : KO.
  Test 3 : Good !
*/

?>